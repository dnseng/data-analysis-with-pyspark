# data-analysis-with-pyspark

## Data Analysis using Pyspark

Now we will build (project goals). We will accomplish it in by completing each task in the project:

- Introduction to the project     
- Importing first csv file of our dataset     
- Learn how to use pyspark sql data frame      
- Apply some queries to extract useful information 
- Importing second csv file of our dataset
- Merge two data frames  and prepare it for more advanced queries
- Visualize your results using matplotlib

